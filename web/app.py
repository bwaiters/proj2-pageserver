from flask import Flask,render_template,request

app = Flask(__name__)

#Error Handling given in Clas Files
@app.errorhandler(403)
def error_403():
    return make_response(render_template("403.html"),403)

@app.errorhandler(404)
def error_404()):
    return make_response(render_template("404.html"),404)

#Orginally Given in Code
@app.route("/")
def hello():
    return "UOCIS docker demo!"

#https://stackoverflow.com/questions/42519329/how-do-python-flask-app-route-sections-execute
@app.route("/trivia.html")
def triviahtml():
    return render_template("trivia.html")

@app.route("/<path:url>")
def urlpath(url):
    if ('..' in url):
        return make_response(render_template("403.html"),403)
    else if ('//'):
        return make_response(render_template("403.html"),403)
    else if ('~'):
        return make_response(render_template("403.html"),403)
    #https://stackoverflow.com/questions/9532499/check-whether-a-path-is-valid-in-python-without-creating-a-file-at-the-paths-ta
    try:
        with open(("/template" + url), 'r', encoding='utf-8') as source:
            return make_response(render_template(url),200)
    except OSError as error:
        return error_404()




if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
